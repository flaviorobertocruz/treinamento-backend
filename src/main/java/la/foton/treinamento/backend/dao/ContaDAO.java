package la.foton.treinamento.backend.dao;

import java.util.List;
import java.util.Optional;

import la.foton.treinamento.backend.entity.Conta;

public interface ContaDAO {

	Integer geraNumero();

	void insere(Conta conta);

	void atualiza(Conta conta);

	Optional<Conta> consultaPorNumero(Integer numero);
	
	List<Conta> consultaPorTitular(String cpfTitular);

	List<Conta> consultaTodas();

}

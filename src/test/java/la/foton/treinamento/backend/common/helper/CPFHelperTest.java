package la.foton.treinamento.backend.common.helper;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CPFHelperTest {

	private String entrada;
	private boolean saidaEsperada;

	public CPFHelperTest(String entrada, boolean saidaEsperada) {
		super();
		this.entrada = entrada;
		this.saidaEsperada = saidaEsperada;
	}

	@Parameters
	public static Collection<Object[]> condicoes() {
		Object saidasEsperadas[][] = { { "00000000000", false }, { "11111111111", false }, { "22222222222", false },
				{ "33333333333", false }, { "44444444444", false }, { "55555555555", false }, { "66666666666", false },
				{ "77777777777", false }, { "88888888888", false }, { "99999999999", false }, { "77276469115", true },
				{ "12345678901", false }, { "123456789", false }, { "2728594430", true } };

		return Arrays.asList(saidasEsperadas);
	}

	@Test
	public void deveValidarOConjuntoDeCPFs() {
		assertEquals(saidaEsperada, CPFHelper.isCpfValido(entrada));
	}

}
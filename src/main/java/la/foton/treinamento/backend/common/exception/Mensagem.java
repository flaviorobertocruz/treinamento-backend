package la.foton.treinamento.backend.common.exception;

public enum Mensagem {

	// Conta
	SALDO_INSUFICIENTE("Saldo Insuficiente"), //
	CONTA_NAO_ENCONTRADA("Conta não encontrada"), //
	CONTA_JA_ENCERRADA("Conta já encerrada"), //
	CONTA_NAO_PODE_SER_ENCERRADA("Conta ão pode ser encerrada pois possui saldo"), //
	TIPO_CONTA_INVALIDO("Tipo de conta inválido"), //
	NAO_EXISTEM_CONTAS("Não existem contas cadastradas"), //
	NAO_EXISTE_CONTA_PARA_TITULAR("Não existe conta para esse titular"), //

	// Cliente
	SITUACAO_CLIENTE_PENDENTE("Situação do cliente está pendente"), //
	CLIENTE_NAO_ENCONTRADO("Cliente não encontrado"), //
	CLIENTE_NAO_PODE_SER_CADASTRADO("Cliente não pode ser cadastrado, dados inválidos"),
	CLIENTE_JA_CADASTRADO("Cliente já cadastrado"), //
	NAO_EXISTEM_CLIENTES("Não existem clientes cadastrados"), //
	NAO_PODE_EXCLUIR_CLIENTE_QUE_POSSUI_CONTA("Cliente possui conta e não pode ser removido"), //
	CPF_INVALIDO("CPF não é válido"); //

	private String texto;

	private Mensagem(String texto) {
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

}

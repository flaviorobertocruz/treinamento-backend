package la.foton.treinamento.backend.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import la.foton.componente.openejbrunner.OpenEjbRunner;
import la.foton.componente.openejbrunner.annotation.DatabaseInit;
import la.foton.componente.openejbrunner.annotation.InjectCdi;
import la.foton.componente.openejbrunner.annotation.InjectEjb;
import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;
import la.foton.treinamento.backend.dao.ClienteDAO;
import la.foton.treinamento.backend.dao.ContaDAO;
import la.foton.treinamento.backend.entity.Cliente;
import la.foton.treinamento.backend.entity.ContaCorrente;
import la.foton.treinamento.backend.entity.SituacaoDoCliente;

@RunWith(OpenEjbRunner.class)
@DatabaseInit
public class ClienteServiceTest {

	@InjectEjb("ClienteServiceLocalBean")
	private ClienteService service;

	@InjectCdi
	private ClienteDAO dao;

	@Test
	public void deveConsultarTodosClientes() {
		try {
			List<Cliente> clientes = service.consultaTodos();

			assertThat(clientes.size(), equalTo(2));
			assertThat(clientes, hasSize(2));
			assertThat(clientes.get(0).getNome(), equalTo("Flavio Roberto"));
			assertThat(clientes.get(0).getCpf(), equalTo("77276469115"));
			assertThat(clientes.get(0).getSituacao(), equalTo(SituacaoDoCliente.ATIVO));
			assertThat(clientes.get(1).getNome(), equalTo("Erica Jordana"));
			assertThat(clientes.get(1).getCpf(), equalTo("02728594430"));
			assertThat(clientes.get(1).getSituacao(), equalTo(SituacaoDoCliente.PENDENTE));
		} catch (NegocioException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void deveConsultarClientePorCpf() {
		try {
			Cliente cliente = service.consultaPorCPF("02728594430");

			assertNotNull(cliente);
			assertThat(cliente.getNome(), equalTo("Erica Jordana"));
			assertThat(cliente.getSituacao(), equalTo(SituacaoDoCliente.PENDENTE));
		} catch (NegocioException e) {
			fail(e.getMensagem().getTexto());
		}
	}

	@Test
	public void deveLancarExcecaoQuandoClienteNaoForEncontrado() {
		Cliente cliente = null;

		try {
			cliente = service.consultaPorCPF("11111111111");

			fail();
		} catch (NegocioException e) {
			assertNull(cliente);
			assertThat(e.getMensagem(), equalTo(Mensagem.CLIENTE_NAO_ENCONTRADO));
		}
	}

	@Test
	public void naoDeveValidarClienteQuandoSituacaoEstaPendente() {
		try {
			Cliente cliente = service.consultaPorCPF("02728594430");

			service.verificaSituacao(cliente);

			fail();
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.SITUACAO_CLIENTE_PENDENTE));
		}
	}

	@Test
	public void deveCadastrarCliente() {
		// Dado que o cpf e o nome sejam válidos e que o cpf já não foi cadastrado.
		try {
			service.cadastraCliente("01234567890", "Nicole Cruz");

			Cliente cadastrado = service.consultaPorCPF("01234567890");

			assertThat(cadastrado.getCpf(), equalTo("01234567890"));
			assertThat(cadastrado.getNome(), equalTo("Nicole Cruz"));
			assertThat(cadastrado.getSituacao(), equalTo(SituacaoDoCliente.PENDENTE));
		} catch (NegocioException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void naoDeveCadastrarClienteQueJaExiste() {
		try {
			service.cadastraCliente("77276469115", "Flavio Roberto");

			fail();
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.CLIENTE_JA_CADASTRADO));
		}
	}

	@Test
	public void deveAtivarUmCliente() {
		try {
			service.cadastraCliente("52161792083", "Nicole Cruz");
			service.ativaCliente("52161792083");

			Cliente cliente = service.consultaPorCPF("52161792083");

			assertNotNull(cliente);
			assertThat(cliente.getNome(), equalTo("Nicole Cruz"));
			assertThat(cliente.getSituacao(), equalTo(SituacaoDoCliente.ATIVO));
		} catch (NegocioException e) {
			fail(e.getMensagem().getTexto());
		}
	}

	@Test
	public void naoDeveCadastrarClienteComCpfNulo() {
		try {
			service.cadastraCliente(null, "Flavio Roberto");
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.CLIENTE_NAO_PODE_SER_CADASTRADO));
		}
	}

	@Test
	public void naoDeveCadastrarClienteComCpfVazio() {
		try {
			service.cadastraCliente("", "Flavio Roberto");
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.CLIENTE_NAO_PODE_SER_CADASTRADO));
		}
	}

	@Test
	public void naoDeveCadastrarClienteComNomeNulo() {
		try {
			service.cadastraCliente("77276469115", null);
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.CLIENTE_NAO_PODE_SER_CADASTRADO));
		}
	}

	@Test
	public void naoDeveCadastrarClienteComNomeVazio() {
		try {
			service.cadastraCliente("77276469115", "");
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.CLIENTE_NAO_PODE_SER_CADASTRADO));
		}
	}

	@Test
	public void deveRemoverUmCliente() {
		Cliente cliente = null;
		try {
			service.cadastraCliente("94836282020", "José da Silva");

			cliente = service.consultaPorCPF("94836282020");
			assertNotNull(cliente);
		} catch (NegocioException e) {
			fail();
		}

		try {
			service.removeCliente("94836282020");
			cliente = service.consultaPorCPF("94836282020");
			fail();
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.CLIENTE_NAO_ENCONTRADO));
		}
	}

	@Ignore
	public void naoDeveRemoverClienteQueAindaPossuiContaVinculada() {
		try {
			service.removeCliente("02728594430");

			fail();
		} catch (NegocioException e) {
			assertThat(e.getMensagem(), equalTo(Mensagem.NAO_PODE_EXCLUIR_CLIENTE_QUE_POSSUI_CONTA));
		}
	}

	protected void mockExamplos() {
		MockitoAnnotations.initMocks(this);
		ContaService contaService = Mockito.spy(ContaService.class);
		ContaDAO contaDAO = Mockito.mock(ContaDAO.class);
		Mockito.doReturn(true).when(contaService).existeContasParaTitular("02728594430");
		Mockito.when(contaService.existeContasParaTitular("02728594430")).thenReturn(true);
		Mockito.when(contaDAO.consultaPorTitular("02728594430")).thenReturn(Arrays.asList(new ContaCorrente()));
	}

}

package la.foton.treinamento.backend.entity;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;

public enum TipoDaConta {

	CORRENTE(1), POUPANCA(2);

	private Integer chave;

	private TipoDaConta(Integer chave) {
		this.chave = chave;
	}

	public Integer getChave() {
		return chave;
	}

	public static TipoDaConta get(Integer tipo) throws NegocioException {
		for (TipoDaConta tipoDaConta : TipoDaConta.values()) {
			if (tipoDaConta.getChave().equals(tipo)) {
				return tipoDaConta;
			}
		}

		throw new NegocioException(Mensagem.TIPO_CONTA_INVALIDO);
	}

}

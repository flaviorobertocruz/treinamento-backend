package la.foton.treinamento.backend.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Alternative;

import la.foton.treinamento.backend.entity.Conta;

@Singleton
@Alternative
@Startup
public class ContaDAOMap implements ContaDAO {

	private Map<Integer, Conta> contas = new HashMap<>();

	@Override
	public Integer geraNumero() {
		return contas.size() + 1;
	}

	@Override
	public void insere(Conta conta) {
		contas.put(conta.getNumero(), conta);
	}

	@Override
	public Optional<Conta> consultaPorNumero(Integer numero) {
		if (contas.containsKey(numero)) {
			return Optional.of(contas.get(numero));
		}

		return Optional.empty();
	}

	@Override
	public List<Conta> consultaPorTitular(String cpfTitular) {
		List<Conta> encontradas = new ArrayList<>();

		for (Conta conta : contas.values()) {
			if (conta.getTitular() != null && conta.getTitular().getCpf() != null
					&& conta.getTitular().getCpf().equals(cpfTitular)) {
				encontradas.add(conta);
			}
		}

		return encontradas;
	}

	@Override
	public void atualiza(Conta conta) {
		contas.put(conta.getNumero(), conta);
	}

	@Override
	public List<Conta> consultaTodas() {
		return new ArrayList<>(contas.values());
	}

}

package la.foton.treinamento.backend.dao;

import java.util.List;
import java.util.Optional;

import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import la.foton.treinamento.backend.entity.Conta;

@Default
public class ContaDAOImpl implements ContaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Integer geraNumero() {
		String sql = "select max(c.numero) from Conta c";

		TypedQuery<Integer> query = em.createQuery(sql, Integer.class);

		Integer numero = query.getSingleResult();

		return numero != null ? numero + 1 : 1;
	}

	@Override
	public void insere(Conta conta) {
		em.persist(conta);
	}

	@Override
	public void atualiza(Conta conta) {
		em.merge(conta);
	}

	@Override
	public Optional<Conta> consultaPorNumero(Integer numero) {
		return Optional.of(em.find(Conta.class, numero));
	}

	@Override
	public List<Conta> consultaPorTitular(String cpfTitular) {
		String sql = "select c from Conta c where c.titular.cpf = :cpfTitular";

		TypedQuery<Conta> query = em.createQuery(sql, Conta.class);

		query.setParameter("cpfTitular", cpfTitular);

		return query.getResultList();
	}

	@Override
	public List<Conta> consultaTodas() {
		return em.createQuery("select c from Conta c", Conta.class).getResultList();
	}

}

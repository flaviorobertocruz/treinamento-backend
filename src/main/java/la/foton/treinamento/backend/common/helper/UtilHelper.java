package la.foton.treinamento.backend.common.helper;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

public class UtilHelper {

	private static final Integer BANCO_FOTON = 123;
	private static final DecimalFormat FORMATO_BANCO = new DecimalFormat("000");
	private static final DecimalFormat FORMATO_AGENCIA = new DecimalFormat("0000");
	private static final DecimalFormat FORMATO_CONTA = new DecimalFormat("0000000");
	private static final DecimalFormat FORMATO_VALOR = new DecimalFormat("0000.00");

	public static int getBancoFoton() {
		return BANCO_FOTON;
	}

	public static String getBancoUniversaFormatado() {
		return FORMATO_BANCO.format(BANCO_FOTON);
	}

	public static String getBancoFormatado(int banco) {
		return FORMATO_BANCO.format(banco);
	}

	public static String getAgenciaFormatada(int agencia) {
		return FORMATO_AGENCIA.format(agencia);
	}

	public static String getContaFormatada(int conta) {
		return FORMATO_CONTA.format(conta);
	}

	public static String getValorFormatado(double valor) {
		return FORMATO_VALOR.format(valor);
	}

	public static String getValorFormatado(BigDecimal valor) {
		return FORMATO_VALOR.format(valor);
	}

	public static Date getDataSemHora(Date dataDeReferencia) {
		Calendar calendario;

		if (dataDeReferencia == null) {
			throw new IllegalArgumentException("Data de referência não pode ser nula.");
		}

		calendario = getCalendario(dataDeReferencia);
		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		calendario.set(Calendar.MILLISECOND, 0);

		return calendario.getTime();
	}

	public static Calendar getCalendario(Date dataDeReferencia) {
		Calendar calendario;

		if (dataDeReferencia == null) {
			throw new IllegalArgumentException("Data de referência não pode ser nula.");
		}

		calendario = Calendar.getInstance();
		calendario.setTime(dataDeReferencia);

		return calendario;
	}

	public static double arredonda(double valor, int numeroDeDecimais) {
		BigDecimal bd = new BigDecimal(Double.toString(valor));

		bd = bd.setScale(numeroDeDecimais, BigDecimal.ROUND_HALF_UP);

		return bd.doubleValue();
	}
}
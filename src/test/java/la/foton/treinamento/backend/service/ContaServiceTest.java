package la.foton.treinamento.backend.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;
import la.foton.treinamento.backend.dao.ContaDAO;
import la.foton.treinamento.backend.entity.Cliente;
import la.foton.treinamento.backend.entity.Conta;
import la.foton.treinamento.backend.entity.ContaCorrente;
import la.foton.treinamento.backend.entity.ContaPoupanca;
import la.foton.treinamento.backend.entity.EstadoDaConta;
import la.foton.treinamento.backend.entity.TipoDaConta;

@RunWith(MockitoJUnitRunner.class)
public class ContaServiceTest {

	@InjectMocks
	private ContaService service;

	@Mock
	private ContaDAO dao;

	@Mock
	private ClienteService clienteService;

	private Cliente titular;

	@Before
	public void setUp() throws NegocioException {
		titular = new Cliente("77276469115", "Flavio Roberto");
	}

	@Test
	public void deveAbrirUmaContaCorrente() {
		Conta contaAberta = null;
		try {
			// Given - Dado que o titular está ativo
			titular.ativa();
			given(clienteService.consultaPorCPF(titular.getCpf())).willReturn(titular);
			given(dao.geraNumero()).willReturn(1);

			// When
			contaAberta = service.abreConta(titular.getCpf(), TipoDaConta.CORRENTE);
		} catch (NegocioException e) {
			fail(e.getMensagem().getTexto());
		}

		// Then
		assertNotNull(contaAberta);
		assertThat(contaAberta.getTipo(), equalTo(TipoDaConta.CORRENTE));
		assertThat(contaAberta.getNumero(), equalTo(1));
		assertThat(contaAberta.getEstado(), equalTo(EstadoDaConta.ATIVA));
		assertThat(contaAberta.getSaldo(), equalTo(500.00));
		assertThat(((ContaCorrente) contaAberta).getLimiteDoChequeEspecial(), equalTo(500.00));
	}

	@Test
	public void deveAbrirUmaContaPoupanca() {
		Conta contaAberta = null;
		try {
			// Given - Dado que o titular está ativo
			titular.ativa();
			given(clienteService.consultaPorCPF(titular.getCpf())).willReturn(titular);
			given(dao.geraNumero()).willReturn(1);

			// When
			contaAberta = service.abreConta(titular.getCpf(), TipoDaConta.POUPANCA);
		} catch (NegocioException e) {
			fail(e.getMensagem().getTexto());
		}

		// Then
		assertNotNull(contaAberta);
		assertThat(contaAberta.getTipo(), equalTo(TipoDaConta.POUPANCA));
		assertThat(contaAberta.getEstado(), equalTo(EstadoDaConta.ATIVA));
		assertThat(contaAberta.getSaldo(), equalTo(0.00));
		assertThat(((ContaPoupanca) contaAberta).getDiaDoAniversario(), equalTo(1));
	}

	@Test
	public void deveLancarUmaExcecaoQuandoConsultaPorUmaContaQueNaoExiste() {
		Conta conta = null;
		try {
			// Given - dado que não exista a conta de número 2

			// When
			conta = service.consultaPorNumero(2);

			fail();
		} catch (NegocioException e) {
			// Then
			assertNull(conta);
			assertThat(e.getMensagem(), equalTo(Mensagem.CONTA_NAO_ENCONTRADA));
		}
	}

	@Test
	public void deveLancarUmaExcecaoQuandoConsultaPorCPFQueNaoExiste() {
		try {
			// Given - dado que não exista contas para o cpf

			// When
			service.consultaPorCpfTitular("1");

			fail();
		} catch (NegocioException e) {
			// Then
			assertThat(e.getMensagem(), equalTo(Mensagem.NAO_EXISTE_CONTA_PARA_TITULAR));
		}
	}

	@Test
	public void deveConsultarContasPorCpf() {
		try {
			// Given - dado que existe contas para um cpf
			Conta contaPoupanca = ContaPoupanca.builder().numero(1).titular(titular).estado(EstadoDaConta.ATIVA)
					.build();
			Conta contaCorrente = ContaCorrente.builder().numero(2).titular(titular).estado(EstadoDaConta.ENCERRADA)
					.build();
			given(dao.consultaPorTitular(titular.getCpf())).willReturn(Arrays.asList(contaPoupanca, contaCorrente));

			// When
			List<Conta> contas = service.consultaPorCpfTitular(titular.getCpf());

			// Then
			assertThat(contas, hasSize(2));
			assertThat(contas, hasItems(contaPoupanca, contaCorrente));
		} catch (NegocioException e) {
			fail();
		}
	}

	@Test
	public void deveConsultarTodasAsContas() {
		List<Conta> contas = new ArrayList<>();

		try {
			// Given - dado que existam 2 contas cadastradas
			Conta contaPoupanca = ContaPoupanca.builder().numero(1).saldo(100.0).estado(EstadoDaConta.ATIVA).build();
			Conta contaCorrente = ContaCorrente.builder().numero(2).saldo(0.0).estado(EstadoDaConta.ENCERRADA).build();
			given(dao.consultaTodas()).willReturn(Arrays.asList(contaPoupanca, contaCorrente));

			// When
			contas = service.consultaTodas();

			// Then - existem duas contas uma conta e outra poupanca
			assertThat(contas.size(), equalTo(2));
			assertThat(contas, hasSize(2));
			assertThat(contas, hasItems(contaPoupanca, contaCorrente));
			assertThat(contas.get(0).getNumero(), equalTo(1));
			assertThat(contas.get(1).getNumero(), equalTo(2));
		} catch (NegocioException e) {
			fail();
		}
	}

	@Test
	public void deveLancarUmaExcecaoQuandoConsultarTodasContasENaoExistirNenhumaCadastrada() {
		List<Conta> contas = new ArrayList<>();
		try {
			// Given - dado que não exista nenhuma conta cadastrada

			// When
			contas = service.consultaTodas();

			fail();
		} catch (NegocioException e) {
			// Then - a lista de contas estará vazia e será lançada a exceção que não
			// existem contas
			assertTrue(contas.isEmpty());
			assertThat(e.getMensagem(), equalTo(Mensagem.NAO_EXISTEM_CONTAS));
		}
	}

	@Test
	public void naoDeveAbrirUmaContaParaUmTitularPendente() {
		try {
			// Given - Dado que o titular está pendente
			given(clienteService.consultaPorCPF(titular.getCpf())).willReturn(titular);
			doThrow(new NegocioException(Mensagem.SITUACAO_CLIENTE_PENDENTE)).when(clienteService)
					.verificaSituacao(titular);

			// When - Abrir uma conta para um titular com estado pendente
			service.abreConta(titular.getCpf(), TipoDaConta.CORRENTE);
			fail();
		} catch (NegocioException e) {
			// Then - A mensagem de situação do cliente pendente deve ser informada
			assertThat(e.getMensagem(), equalTo(Mensagem.SITUACAO_CLIENTE_PENDENTE));
		}
	}

	@Test
	public void deveEncerrarUmaConta() {
		try {
			// Given - uma conta não tenha saldo, e esteja no estado de ativa
			Conta conta = ContaPoupanca.builder().numero(1).saldo(0.0).estado(EstadoDaConta.ATIVA).build();
			given(dao.consultaPorNumero(1)).willReturn(Optional.of(conta));

			// When - encerrar a conta
			service.encerraConta(conta.getNumero());

			// Then
			assertThat(conta.getEstado(), equalTo(EstadoDaConta.ENCERRADA));
			assertThat(conta.getSaldo(), equalTo(0.00));
			verify(dao, times(1)).atualiza(conta);
		} catch (NegocioException e) {
			fail(e.getMensagem().getTexto());
		}
	}
}

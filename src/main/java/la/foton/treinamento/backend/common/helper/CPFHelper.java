package la.foton.treinamento.backend.common.helper;

public class CPFHelper {
	public static boolean isCpfValido(String cpf) {
		if (cpf.length() == 10) {
			cpf = "0" + cpf;
		}

		if (cpf.length() != 11) {
			return false;
		}

		if ((cpf.equals("00000000000")) || (cpf.equals("11111111111")) || (cpf.equals("22222222222"))
				|| (cpf.equals("33333333333")) || (cpf.equals("44444444444")) || (cpf.equals("55555555555"))
				|| (cpf.equals("66666666666")) || (cpf.equals("77777777777")) || (cpf.equals("88888888888"))
				|| (cpf.equals("99999999999"))) {
			return false;
		}

		// Calcula o primeiro dígito
		int soma1 = 0;
		int digito1 = 0;
		int i = 0;
		for (i = 0; i <= 8; i++) {
			soma1 = soma1 + Integer.parseInt(cpf.substring(i, i + 1)) * (10 - i);
		}

		if (soma1 % 11 < 2) {
			digito1 = 0;
		} else {
			digito1 = 11 - (soma1 % 11);
		}

		// Calcula o segundo dígito
		int soma2 = 0;
		int digito2 = 0;
		for (i = 0; i <= 9; i++) {
			soma2 = soma2 + Integer.parseInt(cpf.substring(i, i + 1)) * (11 - i);
		}

		if (soma2 % 11 < 2) {
			digito2 = 0;
		} else {
			digito2 = 11 - (soma2 % 11);
		}

		return ((digito1 == Integer.parseInt(cpf.substring(9, 10)))
				&& (digito2 == Integer.parseInt(cpf.substring(10))));
	}
}

package la.foton.treinamento.backend.dto;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = { "numeroContaDeDebito", "valor", "numeroContaDeCredito" })
public class TransferenciaDTO {

	private Integer numeroContaDeDebito;

	private Double valor;

	private Integer numeroContaDeCredito;

	public Integer getNumeroContaDeDebito() {
		return numeroContaDeDebito;
	}

	public void setNumeroContaDeDebito(Integer numeroContaDeDebito) {
		this.numeroContaDeDebito = numeroContaDeDebito;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Integer getNumeroContaDeCredito() {
		return numeroContaDeCredito;
	}

	public void setNumeroContaDeCredito(Integer numeroContaDeCredito) {
		this.numeroContaDeCredito = numeroContaDeCredito;
	}

}

package la.foton.treinamento.backend.ws;

import java.net.URI;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import la.foton.treinamento.backend.common.exception.NegocioException;
import la.foton.treinamento.backend.dto.TransferenciaDTO;
import la.foton.treinamento.backend.entity.Conta;
import la.foton.treinamento.backend.entity.TipoDaConta;
import la.foton.treinamento.backend.service.ContaService;

@Path("/conta")
@Produces(MediaType.APPLICATION_JSON)
public class ContaWS {

	@EJB
	private ContaService service;

	@GET
	@Path("/{numero}")
	public Response consultaPorNumero(@PathParam("numero") Integer numero) throws NegocioException {
		Conta conta = service.consultaPorNumero(numero);

		return Response.ok().entity(conta).build();
	}

	@PUT
	@Path("/{numero}/encerra")
	public Response encerraConta(@PathParam("numero") Integer numero) throws NegocioException {
		service.encerraConta(numero);

		return Response.ok().build();
	}

	@POST
	@Path("/abertura")
	public Response abreConta(@QueryParam("tipo") Integer tipo, @QueryParam("cpf") String cpf) throws NegocioException {
		Conta conta = null;

		TipoDaConta tipoDaConta = TipoDaConta.get(tipo);

		conta = service.abreConta(cpf, tipoDaConta);

		URI uri = UriBuilder.fromPath("conta/{numero}").build(conta.getNumero());

		return Response.created(uri).entity(conta).type(MediaType.APPLICATION_JSON_TYPE).build();
	}

	@GET
	@Path("/todas")
	public Response consultaTodas() throws NegocioException {
		List<Conta> contas = service.consultaTodas();

		return Response.ok().entity(contas).build();
	}

	@PUT
	@Path("/{numero}/deposita")
	public Response deposita(@PathParam("numero") Integer numero, @QueryParam("valor") Double valor)
			throws NegocioException {
		service.depositaEmConta(numero, valor);

		return Response.ok().build();
	}

	@PUT
	@Path("/{numero}/saca")
	public Response saca(@PathParam("numero") Integer numero, @QueryParam("valor") Double valor)
			throws NegocioException {
		service.sacaEmConta(numero, valor);

		return Response.ok().build();
	}

	@PUT
	@Path("/transfere")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response transfereEntreContas(TransferenciaDTO dto) throws NegocioException {
		service.transfereEntreContas(dto.getNumeroContaDeDebito(), dto.getValor(), dto.getNumeroContaDeCredito());

		return Response.ok().build();
	}

}

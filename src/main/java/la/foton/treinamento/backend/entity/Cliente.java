package la.foton.treinamento.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;
import la.foton.treinamento.backend.common.helper.CPFHelper;
import lombok.Getter;

@Entity
@Table
public class Cliente {

	@Id
	@Column
	@Getter
	private String cpf;

	@Column(nullable = false)
	@Getter
	private String nome;

	@Column
	@Enumerated(EnumType.ORDINAL)
	@Getter
	private SituacaoDoCliente situacao;

	public Cliente() {
		situacao = SituacaoDoCliente.PENDENTE;
	}

	public Cliente(String cpf, String nome) throws NegocioException {
		super();
		this.setCpf(cpf);
		this.nome = nome;
		situacao = SituacaoDoCliente.PENDENTE;
	}

	public void setCpf(String cpf) throws NegocioException {
		if (CPFHelper.isCpfValido(cpf)) {
			this.cpf = cpf;
			return;
		}

		throw new NegocioException(Mensagem.CPF_INVALIDO);
	}

	public void ativa() {
		situacao = SituacaoDoCliente.ATIVO;
	}

}

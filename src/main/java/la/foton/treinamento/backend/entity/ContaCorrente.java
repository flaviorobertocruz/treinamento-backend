package la.foton.treinamento.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "ContaCorrente")
@SuperBuilder
public class ContaCorrente extends Conta {

	@Column(name = "limiteDoChequeEspecial")
	@Getter
	@Setter
	private Double limiteDoChequeEspecial;

	public ContaCorrente() {
		this.tipo = TipoDaConta.CORRENTE;
		this.limiteDoChequeEspecial = 0.0;
	}

	@Override
	public Double getSaldo() {
		return saldo + limiteDoChequeEspecial;
	}

	@Override
	public void debita(Double valor) throws NegocioException {
		if (saldo + limiteDoChequeEspecial < valor) {
			throw new NegocioException(Mensagem.SALDO_INSUFICIENTE);
		}

		saldo -= valor;
	}

}

package la.foton.treinamento.backend.dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import la.foton.componente.openejbrunner.OpenEjbRunner;
import la.foton.componente.openejbrunner.annotation.DatabaseInit;
import la.foton.componente.openejbrunner.annotation.InjectCdi;
import la.foton.treinamento.backend.entity.Conta;

@RunWith(OpenEjbRunner.class)
@DatabaseInit
public class ContaDAOImplTest {

	@InjectCdi
	private ContaDAOImpl dao;

	@Test
	public void deveGerarOProximoNumeroParaAConta() {
		// Dado que já existão 3 contas cadastradas na base.

		// Quando gerar o próximo numero para uma conta
		Integer proximoNumero = dao.geraNumero();

		// Então o proximo numero deverá ser 4
		assertThat(proximoNumero, equalTo(4));
	}

	@Ignore
	public void deveConsultarUmaContaPorNumero() {
		// Dado que a conta 1 já exista na base

		// Quando consulta por numero 1
		Optional<Conta> conta = dao.consultaPorNumero(1);

		// Então
		assertTrue(conta.isPresent());
	}

	@Ignore
	public void deveConsultarTodasAsContas() {
		List<Conta> contas = dao.consultaTodas();

		assertThat(contas, hasSize(3));
	}

}

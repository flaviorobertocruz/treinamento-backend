package la.foton.treinamento.backend.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;

public class ContaTest {

	private Conta conta;

	@Before
	public void setUp() {
		conta = new ContaPoupanca();
		conta.credita(100.00);
	}

	@After
	public void tearDown() {

	}

	@Test
	public void deveCreditarUmValorNaConta() {
		conta.credita(10.0);

		assertEquals(110.0, conta.getSaldo(), 0.0);
	}

	@Test
	public void deveDebitarUmValorNaContaQuePossuiSaldoSuficiente() {
		try {
			conta.debita(15.0);
		} catch (NegocioException e) {
			fail();
		}

		assertEquals(85.0, conta.getSaldo(), 0.0);
	}

	@Test
	public void naoDeveDebitarValorEmContaQueNaoPossuiSaldoSuficiente() {
		try {
			conta.debita(100.01);
			fail();
		} catch (NegocioException e) {
			assertEquals(Mensagem.SALDO_INSUFICIENTE, e.getMensagem());
		}
	}

	@Test
	public void deveTransferirValorEntreContas() {
		Conta contaDeCredito = new ContaPoupanca();

		try {
			conta.transfere(99.99, contaDeCredito);

			assertEquals(0.01, conta.getSaldo(), 0.001);
			assertEquals(99.99, contaDeCredito.getSaldo(), 0.001);
		} catch (NegocioException e) {
			fail();
		}
	}

	@Test
	public void deveEncerrarUmaConta() {
		Conta conta = ContaPoupanca.builder().saldo(0.0).estado(EstadoDaConta.ATIVA).build();

		try {
			conta.encerra();

			assertEquals(EstadoDaConta.ENCERRADA, conta.getEstado());
		} catch (NegocioException e) {
			fail();
		}
	}

	@Test
	public void naoDeveEncerrarUmaContaComSaldo() {
		Conta conta = ContaPoupanca.builder().saldo(0.1).estado(EstadoDaConta.ATIVA).build();

		try {
			conta.encerra();

			fail();
		} catch (NegocioException e) {
			assertEquals(EstadoDaConta.ATIVA, conta.getEstado());
			assertEquals(Mensagem.CONTA_NAO_PODE_SER_ENCERRADA, e.getMensagem());
		}
	}

	@Test
	public void naoDeveEncerrarUmaContaJaEncerrada() {
		Conta conta = ContaCorrente.builder().saldo(0.0).estado(EstadoDaConta.ENCERRADA).build();

		try {
			conta.encerra();

			fail();
		} catch (NegocioException e) {
			assertEquals(EstadoDaConta.ENCERRADA, conta.getEstado());
			assertEquals(Mensagem.CONTA_JA_ENCERRADA, e.getMensagem());
		}
	}

}

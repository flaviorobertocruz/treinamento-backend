package la.foton.treinamento.backend.entity;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@SuperBuilder
public abstract class Conta {

	@Id
	@Getter
	@Setter
	private Integer numero;

	@Column
	@Getter
	private Integer agencia;

	@ManyToOne
	@JoinColumn(name = "cpf")
	@Getter
	@Setter
	private Cliente titular;

	@Column
	@Enumerated(EnumType.ORDINAL)
	@Getter
	private EstadoDaConta estado;

	@Column
	@Getter
	protected Double saldo;

	@Column
	@Convert(converter = TipoDaContaConverter.class)
	@Getter
	protected TipoDaConta tipo;

	@PrePersist
	public void prePersist() {
		this.agencia = 1234;
	}

	public Conta() {
		saldo = 0.0;
	}

	public void credita(Double valor) {
		saldo = saldo + valor;
	}

	public abstract void debita(Double valor) throws NegocioException;

	public void transfere(Double valor, Conta contaDestino) throws NegocioException {
		this.debita(valor);

		contaDestino.credita(valor);
	}

	public void encerra() throws NegocioException {
		if (EstadoDaConta.ENCERRADA.equals(estado)) {
			throw new NegocioException(Mensagem.CONTA_JA_ENCERRADA);
		}

		if (this.saldo > 0) {
			throw new NegocioException(Mensagem.CONTA_NAO_PODE_SER_ENCERRADA);
		}

		encerrada();
	}

	public void ativa() {
		this.estado = EstadoDaConta.ATIVA;
	}

	public void encerrada() {
		estado = EstadoDaConta.ENCERRADA;
	}
}

package la.foton.treinamento.backend.ws;

import org.junit.Ignore;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class ClienteWSTest
{

   @Ignore
   public void deveConsultarTodosClientes()
   {
      RestAssured
         .given()
               .basePath("/treinamento-backend/cliente")
               .port(8080)
               .accept(ContentType.JSON)
         .when()
               .get()
         .then()
               .statusCode(200);
   }

}

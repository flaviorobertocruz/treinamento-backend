package la.foton.treinamento.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "ContaPoupanca")
@SuperBuilder
public class ContaPoupanca extends Conta {

	@Column(name = "diaDoAniversario")
	@Getter
	@Setter
	private Integer diaDoAniversario;

	public ContaPoupanca() {
		this.tipo = TipoDaConta.POUPANCA;
	}

	@Override
	public void debita(Double valor) throws NegocioException {
		if (saldo < valor) {
			throw new NegocioException(Mensagem.SALDO_INSUFICIENTE);
		}

		saldo = saldo - valor;
	}

}

package la.foton.treinamento.backend.service;

import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import la.foton.treinamento.backend.common.exception.Mensagem;
import la.foton.treinamento.backend.common.exception.NegocioException;
import la.foton.treinamento.backend.dao.ContaDAO;
import la.foton.treinamento.backend.entity.Cliente;
import la.foton.treinamento.backend.entity.Conta;
import la.foton.treinamento.backend.entity.ContaCorrente;
import la.foton.treinamento.backend.entity.ContaPoupanca;
import la.foton.treinamento.backend.entity.TipoDaConta;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER) // opcional
public class ContaService {

	@Inject
	private ContaDAO dao;

	@EJB
	private ClienteService clienteService;

	@TransactionAttribute(TransactionAttributeType.REQUIRED) // opcional
	public Conta abreConta(String cpfDoTitular, TipoDaConta tipo) throws NegocioException {
		Cliente titular = clienteService.consultaPorCPF(cpfDoTitular);

		clienteService.verificaSituacao(titular);

		Conta conta = criaConta(tipo, titular);
		conta.setNumero(dao.geraNumero());
		conta.ativa();

		dao.insere(conta);

		return conta;
	}

	public Conta consultaPorNumero(Integer numero) throws NegocioException {
		Optional<Conta> conta = dao.consultaPorNumero(numero);

		if (!conta.isPresent()) {
			throw new NegocioException(Mensagem.CONTA_NAO_ENCONTRADA);
		}

		return conta.get();
	}

	public List<Conta> consultaPorCpfTitular(String cpfTitular) throws NegocioException {
		List<Conta> contas = dao.consultaPorTitular(cpfTitular);

		if (contas.isEmpty()) {
			throw new NegocioException(Mensagem.NAO_EXISTE_CONTA_PARA_TITULAR);
		}

		return contas;
	}
	
	public boolean existeContasParaTitular(String cpfTitular) {
		List<Conta> contas = dao.consultaPorTitular(cpfTitular);
		
		return !contas.isEmpty();
	}

	public void encerraConta(Integer numero) throws NegocioException {
		Conta conta = this.consultaPorNumero(numero);

		conta.encerra();

		dao.atualiza(conta);
	}

	public List<Conta> consultaTodas() throws NegocioException {
		List<Conta> contas = dao.consultaTodas();

		if (contas.isEmpty()) {
			throw new NegocioException(Mensagem.NAO_EXISTEM_CONTAS);
		}

		return contas;
	}

	public void depositaEmConta(Integer numero, Double valor) throws NegocioException {
		Conta conta = this.consultaPorNumero(numero);

		conta.credita(valor);

		dao.atualiza(conta);
	}

	public void sacaEmConta(Integer numero, Double valor) throws NegocioException {
		Conta conta = this.consultaPorNumero(numero);

		conta.debita(valor);

		dao.atualiza(conta);
	}

	public void transfereEntreContas(Integer numeroContaDeDebito, Double valor, Integer numeroContaDeCredito)
			throws NegocioException {
		Conta contaDeDebito = this.consultaPorNumero(numeroContaDeDebito);
		Conta contaDeCredito = this.consultaPorNumero(numeroContaDeCredito);

		contaDeDebito.transfere(valor, contaDeCredito);

		dao.atualiza(contaDeDebito);
		dao.atualiza(contaDeCredito);
	}

	private Conta criaConta(TipoDaConta tipo, Cliente titular) {
		Conta conta = null;

		if (TipoDaConta.CORRENTE.equals(tipo)) {
			conta = new ContaCorrente();
			((ContaCorrente) conta).setLimiteDoChequeEspecial(500.00);
		} else {
			conta = new ContaPoupanca();
			((ContaPoupanca) conta).setDiaDoAniversario(1);
		}

		conta.setTitular(titular);

		return conta;
	}

}